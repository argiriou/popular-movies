# popular-movies

[Demo page](https://argiriou.bitbucket.io/popular-movies/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.1.

## Development server

In order to build the project you should run sequentially the following commands: 
You should have node version > 6.x.x

- npm i         (in order to install the dependencies)
- npm run start (in order to run the server)

And after that you should open the browser on http://localhost:4200
The app will automatically reload if you change any of the source files.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma].
