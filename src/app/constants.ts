export class Constants {
  public static get API_KEY(): string { return '9198fa6d9a9713bc6b03ee9582525917'; }
  public static get PREFIX_URL(): string { return 'https://api.themoviedb.org/3'; }
  public static get MOVIES_URL(): string { return '/discover/movie'; }
  public static get GENRES_URL(): string { return '/genre/movie/list'; }
  public static get LANG_GR(): string { return 'el-GR'; }
  public static get LANG_US(): string { return 'en-US'; }
  public static get POP_DESC(): string { return 'popularity.desc'; }
  public static get POP_ASC(): string { return 'popularity.asc'; }
  public static get VOTE_AVERAGE_DESC(): string { return 'vote_average.desc'; }
  public static get VOTE_AVERAGE_ASC(): string { return 'vote_average.asc'; }
  public static get VOTE_COUNT_DESC(): string { return 'vote_count.desc'; }
  public static get VOTE_COUNT_ASC(): string { return 'vote_count.asc'; }
  public static get IMAGE_PREFIX_S(): string { return 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'; }
  public static get IMAGE_PREFIX_L(): string { return 'https://image.tmdb.org/t/p/w600_and_h900_bestv2'; }

  public static get SORTING_PROPS(): object {
    return {
      'pop_desc': {
        name: 'Most popular',
        code: [Constants.POP_DESC]
      },
      'pop_asc': {
        name: 'Least popular',
        code: [Constants.POP_ASC]
      },
      'vote_average_desc': {
        name: 'Best rating',
        code: [Constants.VOTE_AVERAGE_DESC]
      },
      'vote_average_asc': {
        name: 'Worst rating',
        code: [Constants.VOTE_AVERAGE_ASC]
      },
      'vote_count_desc': {
        name: 'Most votes',
        code: [Constants.VOTE_COUNT_DESC]
      },
      'vote_count_asc': {
        name: 'Least votes',
        code: [Constants.VOTE_COUNT_ASC]
      }
    };
  }
}
