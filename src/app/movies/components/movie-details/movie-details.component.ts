import { Component, OnInit } from '@angular/core';
import { Constants } from '../../../constants';
import { MoviesService } from '../../services/movies.service';
import {Genre, GenresResponse, Movie} from '../../types/movie.dt';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})

export class MovieDetailsComponent implements OnInit {
  selectedMovie: Movie;
  movieGenres: any;
  Math: any;
  stars: Array<number>  = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(
    private moviesService: MoviesService
  ) {
    this.Math = Math;
  }

  ngOnInit(): void {
    this.onInit();
  }

  onInit(): void {
    this.moviesService.change.subscribe(movie => {
      this.selectedMovie = movie;
      if (this.selectedMovie.poster_path.indexOf(Constants.IMAGE_PREFIX_L) === -1) {
        this.selectedMovie.poster_path = `${Constants.IMAGE_PREFIX_L}${movie.poster_path}`;
      }
    });

    /* Call the fetchCategories() and subscribe to the event */
    this.moviesService.fetchCategories()
      .subscribe(
        data => this.movieGenres = data,
        () => {
          console.log('ERORRRRRRRRR');
        });
    }

  filterCategories(movieGenres: Array<Genre>, genreId: number): string {
    return this.moviesService.filterCategories(movieGenres, genreId);
  }

}
