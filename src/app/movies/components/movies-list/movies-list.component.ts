import { Component, Input, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Constants } from '../../../constants';
import { Movie } from '../../types/movie.dt';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})

export class MoviesListComponent implements OnInit {
  movies: Array<Movie> = [];
  selectedMovie: Movie;
  page: number = 0;
  cssClassSelector: string = '.movies-list__scroll';
  @Input() searchTerm: string = '';
  moviesFetched: number;

  objectKeys = Object.keys;
  sortingProps: object = Constants.SORTING_PROPS;
  sortingCode: string = Constants.POP_DESC;

  constructor(
    private moviesService: MoviesService
  ) {}

  ngOnInit(): void {
    this.initData();
  }

  onClickMovie(movie: Movie): void {
    this.selectedMovie = movie;
    this.moviesService.onSelectMovie(movie);
  }

  initData(): void {
    this.moviesService.fetchMovies(++this.page, this.sortingCode)
      .subscribe(
      data => {
        this.movies = [...this.movies, ...data];
        this.moviesFetched = this.movies.length;
      },
      error => {
        console.log('ERORRRRRRRRR++', error.error || '');
      });
  }

  setSortingCode(code: string): void {
    this.sortingCode = code;
    this.movies = [];
    this.page = 0;
    this.initData();
  }

}
