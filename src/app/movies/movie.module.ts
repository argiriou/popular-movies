import { NgModule } from '@angular/core';
import {MoviesListComponent} from './components/movies-list/movies-list.component';
import {MovieDetailsComponent} from './components/movie-details/movie-details.component';
import {SearchPipe} from './pipes/search.pipe';
import {MoviesService} from './services/movies.service';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MoviePageComponent} from './pages/movie-page/movie-page.component';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    MoviesListComponent,
    MovieDetailsComponent,
    SearchPipe,
    MoviePageComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    CommonModule,
    InfiniteScrollModule
  ],
  providers: [
    MoviesService
  ],
  exports: [
    MoviePageComponent
  ]
})
export class MovieModule { }
