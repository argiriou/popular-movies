import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {MoviesListComponent} from '../../components/movies-list/movies-list.component';
import {MovieDetailsComponent} from '../../components/movie-details/movie-details.component';
import {FormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {HttpClientModule} from '@angular/common/http';
import {SearchPipe} from '../../pipes/search.pipe';
import {MoviesService} from '../../services/movies.service';
import {MoviePageComponent} from './movie-page.component';
import {CommonModule} from '@angular/common';

describe('MoviePageComponent', () => {

  let fixture: ComponentFixture<MoviePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MoviesListComponent,
        MovieDetailsComponent,
        SearchPipe,
        MoviePageComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        CommonModule,
        InfiniteScrollModule
      ],
      providers:    [
        { provide: MoviesService,
          useValue: jasmine.createSpyObj('MoviesService', [
            'selectedMovie',
            'onSelectMovie',
            'change'
          ])
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MoviePageComponent);
  }));
  it('should create the app', async(() => {
    // const fixture = TestBed.createComponent(MoviePageComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Test title'`, async(() => {
    // const fixture = TestBed.createComponent(MoviePageComponent);
    const app = fixture.debugElement.componentInstance;
    app.title = 'Test title';
    expect(app.title).toEqual('Test title');
  }));
  // it('should render title in a h1 tag', async(() => {
  //   // const fixture = TestBed.createComponent(MoviePageComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Popular movies app');
  // }));
});
