import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable} from 'rxjs/Observable';
import { Constants } from '../../constants';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Genre, GenresResponse, Movie} from '../types/movie.dt';

@Injectable()
export class MoviesService {
  selectedMovie = {} as Movie;

  constructor(
    private http: HttpClient
  ) {}

  @Output() change: EventEmitter<any> = new EventEmitter();

  onSelectMovie(movie: Movie): void {
    this.selectedMovie = movie;
    this.change.emit(this.selectedMovie);
  }

  fetchCategories(): Observable<Array<Genre>> {
    const url = `${Constants.PREFIX_URL}${Constants.GENRES_URL}` +
      `?api_key=${Constants.API_KEY}&language=${Constants.LANG_US}`;
    return this.http
      .get(url)
      .map(
        data => {
          return data['genres'].map(item => {
            return item;
          });
        }
      )
      .catch(
        (error: HttpErrorResponse) => {
          return Observable.throw(error.error || 'Server error');
        }
      );
  }

  filterCategories(movieGenres: Array<Genre>, genreId: number): string {
    if (!movieGenres || !genreId) {
      return;
    }
    return movieGenres
      .filter(movieGenre => movieGenre.id === genreId) // Filter the total genres with the specific movie genre
      .map(genre => genre.name) // Map the name of the genre
      .toString(); // Return the string name of the genre
  }

  fetchMovies(page: number, sortBy: string): Observable<Array<Movie>> {
    const url = `${Constants.PREFIX_URL}${Constants.MOVIES_URL}` +
      `?api_key=${Constants.API_KEY}&sort_by=${sortBy}&page=${page}`;
    return this.http
      .get(url)
      .map(
        data => {
          return data['results'].map(item => {
            return item;
          });
        }
      )
      .catch(
        (error: HttpErrorResponse) => {
          return Observable.throw(error.error || 'Server error');
        }
      );
  }

}
